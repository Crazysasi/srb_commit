import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { SidenavComponent } from './Components/sidenav/sidenav.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

 

import { ReactiveFormsModule } from '@angular/forms';

import { MatModule } from './Modules/mat/mat.module';

// import { CustomerDetailsModule } from './customer-details/customer-details.module';

import { CustomerTableModule } from './Modules/Customer/customer-table.module';

import { ProductTableModule } from './Modules/Products/product-table.module';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SignUpComponent } from './Components/sign-up/sign-up.component';

@NgModule({

  declarations: [

    AppComponent,

    SidenavComponent,

    SignUpComponent,

  ],

  imports: [

    BrowserModule,

    AppRoutingModule,

    BrowserAnimationsModule,

    ReactiveFormsModule,

    MatModule,

    ProductTableModule,

    HttpClientModule

  ],

 

  providers: [],

  entryComponents: [SidenavComponent],

  bootstrap: [AppComponent]

})

 

export class AppModule { }

