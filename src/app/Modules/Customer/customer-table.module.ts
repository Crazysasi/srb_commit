import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';

import { CustomerTableComponent } from './Components/customer-table/customer-table/customer-table.component';

 

import { AgGridModule } from 'ag-grid-angular';

import { CustomisedEditButtonComponent } from './Components/customer-table/customised-edit-button/customised-edit-button.component';

import { MatModule } from '../mat/mat.module';

import { ReactiveFormsModule } from '@angular/forms';

// import { GridDataService } from '../shared/grid-data.service';

import { CustomerUpdateDetailsComponent } from './Components/customer-table/customer-update-details/customer-update-details.component';

import { CustomerRoutingModule } from './customer-routing.module';

@NgModule({

  declarations: [CustomerTableComponent, CustomisedEditButtonComponent, CustomerUpdateDetailsComponent],

  imports: [

    CommonModule,

    MatModule,

    ReactiveFormsModule,

    CustomerRoutingModule,

    AgGridModule.withComponents([CustomisedEditButtonComponent]

      )

  ],

  exports:[

    CustomerTableComponent,

  ],

  providers:[

  ],

  entryComponents: [CustomerUpdateDetailsComponent]

})

export class CustomerTableModule { constructor() {

  console.log('CustomerTableModule loaded.');

}}