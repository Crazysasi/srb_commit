import { Component, OnInit, ViewChild, AfterViewChecked, TemplateRef } from '@angular/core';

import { ICellRendererAngularComp } from 'ag-grid-angular';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { CustomerUpdateDetailsComponent } from '../customer-update-details/customer-update-details.component';

import { CustomerTableComponent } from '../customer-table/customer-table.component';

import { forwardRef } from '@angular/core';

 

@Component({

  selector: 'app-customised-edit-button',

  templateUrl: './customised-edit-button.component.html',

  styleUrls: ['./customised-edit-button.component.css']

})

export class CustomisedEditButtonComponent implements ICellRendererAngularComp {

  //@ViewChild(forwardRef(() => CustomerTableComponent)) singleRowData: CustomerTableComponent;

  params;

  label: string;

  isavailable = true;

  test =  "s"

  gridApi: any

  gridColumnApi : any

  agInit(params): void {

    this.params = params;

    this.label = this.params.label || null;

    this.gridApi = params.api;

    this.gridColumnApi = params.columnApi;

  }

 

  constructor(public dialog: MatDialog) { }

  openDialog() {

    const dialogRef = this.dialog.open(CustomerUpdateDetailsComponent, {

      width: '700px',

      height: 'auto',

      data: {

        name: this.params.data.name,

        email :this.params.data.email,

        phone : this.params.data.phone,

        age : this.params.data.age,

        city : this.params.data.city,

        pincode : this.params.data.pincode

      }

    });

   

    dialogRef.afterClosed().subscribe(result => {

      console.log('The dialog was closed');

    });

    this.isavailable = true;

  }

  deleteRow(){

    console.log(this.gridApi.getSelectedRows());

    var selectedData = this.gridApi.getSelectedRows();

    var res = this.gridApi.updateRowData({ remove: selectedData });

  }

  refresh(params?: any): boolean {

    return true;

  }

  // onClick($event) {

  //   console.log($event);

   

  //   if (this.params.onClick instanceof Function) {

  //     // put anything into params u want pass into parents component

  //     const params = {

  //       event: $event,

  //       rowData: this.params.node.data

  //       // ...something

  //     }

  //     console.log('params',this.params.data)

  //     this.params.onClick(params);

 

  //   }

  // }

  // ngAfterViewInit() {

  //   console.log('rowdatac', this.singleRowData)

  // }

  // ngAfterViewChecked()           {

  //   console.log('rowdatacc', this.singleRowData)

  // }

   

 

}