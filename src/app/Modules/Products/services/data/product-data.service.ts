import { Injectable } from '@angular/core';
import { resolve } from 'url';
import { HttpClient } from '@angular/common/http';
import ProductJson from 'app/Modules/Products/services/json/product.json';

@Injectable({
  providedIn: 'root'
})
export class ProductDataService {

  constructor(private http:HttpClient) { }

  getProductData(){
    return new Promise((resolve,reject)=>{
      resolve(ProductJson);      
    })
  }
}
