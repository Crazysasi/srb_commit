import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';

import { ProductTableComponent } from './Components/product-table/product-table/product-table.component';

 

const routes: Routes = [

  {

    path: '',

    component: ProductTableComponent,

  }

];

 

@NgModule({

  imports: [RouterModule.forChild(routes)],

  exports: [RouterModule]

})

export class ProductRoutingModule { constructor() {

  console.log('ProductRoutingModule loaded.');

}}

 