import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';

import { ProductTableComponent } from './Components/product-table/product-table/product-table.component';

import { ProductEditButtonComponent } from './Components/product-table/product-edit-button/product-edit-button.component';

import { ProductUpdateDetailsComponent } from './Components/product-table/product-update-details/product-update-details.component';

import { MatModule } from '../mat/mat.module';

import { ReactiveFormsModule } from '@angular/forms';

import { AgGridModule } from 'ag-grid-angular';

import { ProductUserDetailsComponent } from './Components/product-table/product-user-details/product-user-details.component';

import { ProductRoutingModule } from './product-routing.module';

 

@NgModule({

  declarations: [ProductTableComponent, ProductEditButtonComponent, ProductUpdateDetailsComponent, ProductUserDetailsComponent],

  imports: [

    CommonModule,

    MatModule,

    ProductRoutingModule,

    ReactiveFormsModule,

    AgGridModule.withComponents([ProductEditButtonComponent]

      )

  ],exports :[ProductTableComponent,],

  entryComponents: [ProductUpdateDetailsComponent, ProductUserDetailsComponent]

})

export class ProductTableModule { }