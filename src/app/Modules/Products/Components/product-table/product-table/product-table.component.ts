import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductEditButtonComponent } from '../product-edit-button/product-edit-button.component';
import { HttpClient } from '@angular/common/http';
// import { AllCommunityModules, Module } from "@ag-grid-community/all-modules";
import { ProductUserDetailsComponent } from '../product-user-details/product-user-details.component';
import { ProductViewService } from '../../../services/view/product-view.service';
@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css']
})
export class ProductTableComponent implements OnInit {
  frameworkComponents: any;
  rowDataClicked1 = {};
  rowData: any;
  rowDataClicked2 = {};
  private gridApi
  private columnDefs;
  private gridColumnApi
  private getRowNodeId;
  private rowSelection;
  // public modules: Module[] = AllCommunityModules;
  constructor(private httpservice: HttpClient,private productViewService:ProductViewService) {
    this.frameworkComponents = {
      productEdit: ProductEditButtonComponent
    };
    this.rowSelection = "single"
    this.columnDefs = [
      { headerName: 'Product ID', field: 'product_id' },
      { headerName: 'Product Name', field: 'product_name' },
      { headerName: 'Product Price', field: 'product_price' },
      {
        headerName: 'Action',
        cellRenderer: "productEdit",
        cellRendererParams: {
          from:'action'
        }
      },
      {
        headerName: 'Customers',
        cellRenderer: "productEdit",
        cellRendererParams: {
          from:'customer'
        }
      }
    ];
  }
  onRowClick(event: any): void {
    
    console.log(event.rowIndex);
  }
  ngOnInit() {
    this.getProductData();
    // this.httpservice.get('../assets/product.json').subscribe(
    //   data => {
    //     this.rowData = data;
    //   },
    // )
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  singleRowData: any
  getRow() {
    var selectedData = this.gridApi.getSelectedRows();
    var res = this.gridApi.updateRowData({ remove: selectedData });
  }

  onBtnClick1(e) {
    console.log(e.rowData.age);
    this.rowDataClicked1 = e.rowData;
  }

  getProductData(){
    this.productViewService.getProductData().then((res)=>{
      console.log(res);
      this.rowData = res;
    })
  }

}

