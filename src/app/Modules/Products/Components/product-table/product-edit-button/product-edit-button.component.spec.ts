import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductEditButtonComponent } from './product-edit-button.component';

describe('ProductEditButtonComponent', () => {
  let component: ProductEditButtonComponent;
  let fixture: ComponentFixture<ProductEditButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductEditButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductEditButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
