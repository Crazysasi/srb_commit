import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';

import { SidenavComponent } from './Components/sidenav/sidenav.component';
import { SignUpComponent } from './Components/sign-up/sign-up.component';
// import { CustomerTableModule } from './customer-table/customer-table.module';

 

const routes: Routes = [

  {path : '', component: SignUpComponent},

  //  {path : 'customer',   loadChildren: 'src/app/customer-table/customer-table.module#CustomerTableModule', component: SidenavComponent,}

  { path: 'customer', loadChildren: () => import('./Modules/Customer/customer-table.module').then(m => m.CustomerTableModule)},

  { path: 'products', loadChildren: () => import('./Modules/Products/product-table.module').then(m => m.ProductTableModule)},

];

 

@NgModule({

  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule]

})

 

export class AppRoutingModule {

  

}